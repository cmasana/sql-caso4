/* Muestra un listado de los trabajadores que contienen 'mont' */
SELECT * FROM employees
WHERE email LIKE '%mont%';

/* Muestra un listado de los empleados cuyo apellido no contiene 'Ma' */
SELECT * FROM employees
WHERE last_name NOT LIKE 'Ma%';

/* Muestra un listado de los trabajadores cuyo apellido contiene 'Vizcarr' */
SELECT * FROM employees
WHERE last_name LIKE 'Vizcarr_';

/* Muestra un listado de los trabajadores cuyo nombre no contiene 'arc' */
SELECT * FROM employees
WHERE first_name NOT LIKE '_arc';

/* Muestra un listado de los trabajadores cuyo salario termina en 0 */
SELECT first_name, last_name, salary FROM employees
WHERE salary LIKE '%0';

/* Muestra un listado de los trabajadores cuyo salario no empieza por 2 */
SELECT first_name, last_name, salary FROM employees
WHERE salary NOT LIKE '2%';