/* Mostramos los nombres de los clientes y el nombre de los empleados unidos en 
una sola consulta. */
SELECT CLIENTS.NOM FROM CLIENTS
UNION
SELECT EMPLOYEES.FIRST_NAME FROM EMPLOYEES;
    
/* Mostramos los nombres de los clientes y el nombre de los empleados que no tienen un departamento
unidos en una sola consulta. */
SELECT CLIENTS.NOM FROM CLIENTS
UNION
SELECT EMPLOYEES.FIRST_NAME FROM EMPLOYEES
    WHERE EMPLOYEES.DEPARTMENT_ID IS NULL;

/* Mostramos todas las comandas y detalles en una sola consulta. */
SELECT COMANDA.COM_NUM FROM COMANDA
UNION
SELECT DETALL.DETALL_NUM FROM DETALL;

/* Mostramos los nombres que estan en la tabla clientes y ademas en la tabla empleados. */
SELECT CLIENTS.NOM FROM CLIENTS
INTERSECT
SELECT EMPLOYEES.FIRST_NAME FROM EMPLOYEES;

/* Mostramos los nombres de la tabla empleados quitando el que se encuentra repetido en 
la segunda tabla, como veis 'Marc', no aparece.*/
SELECT EMPLOYEES.FIRST_NAME FROM EMPLOYEES
MINUS
SELECT CLIENTS.NOM FROM CLIENTS;












