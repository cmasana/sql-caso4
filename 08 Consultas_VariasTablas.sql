/*Hacemos una consulta de la tabla contratos y empleados.*/
SELECT 
    start_date, end_date, job_history.employee_id, job_history.department_id
    from job_history;
    
SELECT 
     first_name, phone_number, employees.employee_id
    from employees;
    

/*Hacemos una consulta de la tabla empleados y contratos.*/
SELECT 
    start_date, end_date, job_history.employee_id, job_history.department_id,
     first_name, phone_number, employees.employee_id
    from job_history, employees;

/*Hacemos una consulta de las tres tablas juntas. Y observamos las tablas que teienen que coincidir para aplicar el filtro.*/
SELECT first_name, phone_number, employees.employee_id,
    department_name, departments.department_id, 
    start_date, end_date, job_history.employee_id, job_history.department_id
    from employees, departments, job_history
        ORDER BY employees.first_name;

/*Realizamos el filtro sobre la información que antes hemos visto. Observamos que para hacer el filtro tiene que coincidir
el número del empleado de la tabla empleados con el número de empleado de la tabla contratos.I el núemero de departamento 
tiene que coincidir con el número de departamento de contrato. También ordenamos por nombre.*/
SELECT first_name, phone_number, employees.employee_id,
    department_name, departments.department_id, 
    start_date, end_date, job_history.employee_id, job_history.department_id
    from employees, departments, job_history
        where employees.employee_id = job_history.employee_id
        AND departments.department_id = job_history.department_id
        ORDER BY employees.first_name;


        






