-- Fecha del sistema en formato numérico 
SELECT TO_CHAR(sysdate,'dd MM  yyyy')
"Fecha de hoy v1" FROM DUAL; 

--Fecha del sistema en otro formato
SELECT TO_CHAR(sysdate,'day month year')
"Fecha de hoy v2" FROM DUAL;

/*Listamos las comandas con su respectivo número y trabajador
modificando el formato del total de la comanda*/
SELECT COM_NUM,EMPLOYEE_ID, TO_CHAR(TOTAL, '$9,999.999'),
    TO_CHAR(TOTAL, '$99,999.9')
    FROM COMANDA;
    
/*Convertimos la fecha de la comanda a varchar2 en diferente formato*/
SELECT Employee_ID,COM_NUM, TO_CHAR(COM_DATA, 'fmDD Month YYYY') 
FROM COMANDA;

--Listado de las comandas hechas antes del año 1987.
SELECT Employee_ID,COM_NUM,COM_TIPUS, COM_DATA FROM COMANDA
WHERE COM_DATA < to_date('01-01-87');

--Convertimos una String en un número.
SELECT TO_NUMBER ('233.87', '99999') "Número" FROM DUAL;

--Tenemos que indicar el formato del número correctamente
SELECT TO_NUMBER ('233.87', '999.99') "Número" FROM DUAL;


