/* Calcula la media de los importes de la comanda 616 */
select AVG(import) from detall
where com_num=616;

/* Muestra el número total de filas en la tabla comanda */
select count(*) from comanda;

/* Muestra el número de filas que tienen datos en la columna com_tipus */
select count(com_tipus) from comanda;

/* Muestra el total más alto de las comandas */
select max(total) from comanda;

/* Muestra el total más alto de las comandas */
select min(total) from comanda;

/* Muestra el resultado de la suma de los totales de las comandas */
select sum(total) from comanda;

/* Muestra la variación de los salarios mínimos de cada cargo 
 * si el valor está muy alejado de la media muestra un valor alto
 */
select variance(min_salary) from jobs;
