/* Muestra el salario medio de los trabajadores que hay en cada departamento */
select department_id, avg(salary) from employees
group by department_id;

/* Muestra el salario medio de los trabajadores que hay en cada departamento
 * mostrando el nombre de cada departamento
 */
select employees.department_id, avg(employees.salary), departments.department_name from employees,departments
where employees.department_id = departments.department_id
group by employees.department_id, departments.department_name
order by employees.department_id;

/* Muestra el salario medio, mayor de 1500, de los trabajadores que hay en cada departamento
 * mostrando el nombre de cada departamento
 */
select employees.department_id, avg(employees.salary), departments.department_name from employees,departments
where employees.department_id = departments.department_id
group by employees.department_id, departments.department_name
having avg(employees.salary) > 1500
order by employees.department_id;
