/* Listado de trabajadores que cobran entre 1€ y 1900€ */
select FIRST_NAME, SALARY from EMPLOYEES
WHERE SALARY BETWEEN 1 AND 1900;

/* Listado de trabajadores que cobran 800€ o 2000€ */
select FIRST_NAME, LAST_NAME, SALARY from EMPLOYEES
WHERE SALARY=800 OR SALARY=2000;

/* Listado de empleados que cobran 800€ o 2000€ CON IN */
select FIRST_NAME, SALARY from EMPLOYEES
WHERE SALARY IN (800,2000);

/* Listado de trabajadores que tienen un salario de 800€ a 1500€ */
select FIRST_NAME, LAST_NAME, SALARY from EMPLOYEES
WHERE SALARY>=800 AND SALARY<=1500;

/* Sería equivalente a realizar la consulta usando BETWEEN así:
NOTA: BETWEEN tiene en cuenta el valor que introduces, en AND tienes que poner el "=" */
select FIRST_NAME, SALARY from EMPLOYEES
WHERE SALARY BETWEEN 800 AND 1500;

/* Listado de los salarios máximos de cada cargo y restamos 1000 a todos */
select JOB_TITLE, MAX_SALARY, MAX_SALARY-1000 from JOBS;
