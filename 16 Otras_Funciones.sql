--Sustituimos el estado con un codigo para cada caso. CA=1 MN=2 y NT=3
SELECT NOM, CIUTAT, ESTAT, DECODE(UPPER(ESTAT), 'CA', 1, 'MN', 2, 'NT', 3) "Codigo" 
FROM CLIENTS;

--Listado de el numero de producto cuyos sean menores a 2000000
--y su descripción en decimal y ordenados.
SELECT PROD_NUM, DESCRIPCIO, DUMP(DESCRIPCIO,10,1,10) "Decimal"
    FROM PRODUCTE
    WHERE PROD_NUM<200000
    ORDER BY PROD_NUM;

--Listado de el numero de producto y su descripción en octal y ordenados.    
SELECT PROD_NUM, DESCRIPCIO, DUMP (DESCRIPCIO,1008, 10, 13) "Octal"
    FROM PRODUCTE
    ORDER BY PROD_NUM ASC;
    
--Contamos los BYTES de los nombres de los clienes cuyos tengan un 
--credito límite mayor a 5000
SELECT NOM, VSIZE(NOM) BYTES_NOM, CIUTAT, LIMIT_CREDIT FROM CLIENTS
WHERE LIMIT_CREDIT>5000;

--Mostramos el usuario que está conectado y su identificador
select USER, UID FROM DUAL;

--Otra forma de mostrar el usuario activo
SHOW USER;
