-- TABLA REGIONS
insert into REGIONS
values (1,'Europa');
insert into REGIONS
values (2,'America');
insert into REGIONS
values (3,'Asia');

-- TABLA COUNTRIES
insert into COUNTRIES (COUNTRY_ID, COUNTRY_NAME, REGION_ID)
values ('AN', 'Andorra', 1);
insert into COUNTRIES (COUNTRY_ID, COUNTRY_NAME, REGION_ID)
values ('BR', 'Brasil', 2);
insert into COUNTRIES (COUNTRY_ID, COUNTRY_NAME, REGION_ID)
values ('JP', 'Japon', 3);

-- TABLA LOCATIONS
insert into LOCATIONS (LOCATION_ID,STREET_ADDRESS,POSTAL_CODE,CITY,STATE_PROVINCE,COUNTRY_ID)
values (1,'Carrer de Josep Rossel','AD400','La Massana','Terol','AN');
insert into LOCATIONS (LOCATION_ID,STREET_ADDRESS,POSTAL_CODE,CITY,STATE_PROVINCE,COUNTRY_ID)
values (2,'Rua da Carioca','22251-050','Rio de Janeiro','Rio de Janeiro','BR');
insert into LOCATIONS (LOCATION_ID,STREET_ADDRESS,POSTAL_CODE,CITY,STATE_PROVINCE,COUNTRY_ID)
values (3,'Kawasaki Street','43870','Ikutora','Hokkaido','JP'); 

-- DESACTIVAR CONSTRAINT PORQUE NO HAY EMPLEADOS EN LA TABLA CORRESPONDIENTE
ALTER TABLE DEPARTMENTS
    DISABLE CONSTRAINT dept_mgr_fk;

--TABLA DEPARTMENTS
insert into DEPARTMENTS (DEPARTMENT_ID, DEPARTMENT_NAME, MANAGER_ID, LOCATION_ID)
values (1, 'Administracion', NULL, 1);
insert into DEPARTMENTS (DEPARTMENT_ID, DEPARTMENT_NAME, MANAGER_ID, LOCATION_ID)
values (2, 'Contabilidad', NULL, 2);
insert into DEPARTMENTS (DEPARTMENT_ID, DEPARTMENT_NAME, MANAGER_ID, LOCATION_ID)
values (3, 'Recepcion', NULL, 3);

-- TABLA JOBS
insert into JOBS (JOB_ID,JOB_TITLE,MIN_SALARY,MAX_SALARY)
values ('1','Administrativo',1500,2500);
insert into JOBS (JOB_ID,JOB_TITLE,MIN_SALARY,MAX_SALARY)
values ('2','Contable',1000,2000);
insert into JOBS (JOB_ID,JOB_TITLE,MIN_SALARY,MAX_SALARY)
values ('3','Recepcionista',1000,2000);

-- TABLA EMPLOYEES
insert into EMPLOYEES (EMPLOYEE_ID, FIRST_NAME, LAST_NAME, EMAIL, PHONE_NUMBER, HIRE_DATE, JOB_ID, SALARY, COMMISSION_PCT, MANAGER_ID, DEPARTMENT_ID)
values (1, 'Joan', 'Vila', 'joanvila2@montsia.org', '648644510', TO_DATE('20-08-2014', 'dd-MM-yyyy'), '1', 2000, NULL, 1, 1);
insert into EMPLOYEES (EMPLOYEE_ID, FIRST_NAME, LAST_NAME, EMAIL, PHONE_NUMBER, HIRE_DATE, JOB_ID, SALARY, COMMISSION_PCT, MANAGER_ID, DEPARTMENT_ID)
values (11, 'Pau', 'Princep', 'pauprincep@montsia.org', '123789654', TO_DATE('29-06-2015', 'dd-MM-yyyy'), '1', 1500, NULL, NULL, 1);
insert into EMPLOYEES (EMPLOYEE_ID, FIRST_NAME, LAST_NAME, EMAIL, PHONE_NUMBER, HIRE_DATE, JOB_ID, SALARY, COMMISSION_PCT, MANAGER_ID, DEPARTMENT_ID)
values (2, 'Carlos', 'Masana', 'carlosmasana@montsia.org', '789654312', TO_DATE('24-11-2014', 'dd-MM-yyyy'), '2', 2000, NULL, 2, 2);
insert into EMPLOYEES (EMPLOYEE_ID, FIRST_NAME, LAST_NAME, EMAIL, PHONE_NUMBER, HIRE_DATE, JOB_ID, SALARY, COMMISSION_PCT, MANAGER_ID, DEPARTMENT_ID)
values (22, 'Victor', 'Marqués', 'victormarques@montsia.org', '988372890', TO_DATE('04-12-2015', 'dd-MM-yyyy'), '2', 800, NULL, NULL, 2);
insert into EMPLOYEES (EMPLOYEE_ID, FIRST_NAME, LAST_NAME, EMAIL, PHONE_NUMBER, HIRE_DATE, JOB_ID, SALARY, COMMISSION_PCT, MANAGER_ID, DEPARTMENT_ID)
values (3, 'Alex', 'Pascual', 'alexpascual@montsia.org', '908456712', TO_DATE('24-10-2015', 'dd-MM-yyyy'), '3', 2000, NULL, 3, 3);
insert into EMPLOYEES (EMPLOYEE_ID, FIRST_NAME, LAST_NAME, EMAIL, PHONE_NUMBER, HIRE_DATE, JOB_ID, SALARY, COMMISSION_PCT, MANAGER_ID, DEPARTMENT_ID)
values (33, 'Marc', 'Vizcarro', 'marcvizcarro@montsia.org', '908459999', TO_DATE('24-10-2015', 'dd-MM-yyyy'), '3', 1500, NULL, NULL, 3);

-- TABLA JOB_HISTORY
insert into JOB_HISTORY (EMPLOYEE_ID,START_DATE,END_DATE,JOB_ID,DEPARTMENT_ID)
values (1,TO_DATE('20-08-2014', 'dd-MM-yyyy'),TO_DATE('20-08-2021', 'dd-MM-yyyy'),1,1);
insert into JOB_HISTORY (EMPLOYEE_ID,START_DATE,END_DATE,JOB_ID,DEPARTMENT_ID)
values (11,TO_DATE('29-06-2015', 'dd-MM-yyyy'),TO_DATE('29-06-2020', 'dd-MM-yyyy'),1,1);
insert into JOB_HISTORY (EMPLOYEE_ID,START_DATE,END_DATE,JOB_ID,DEPARTMENT_ID)
values (2,TO_DATE('24-11-2012', 'dd-MM-yyyy'),TO_DATE('23-11-2014', 'dd-MM-yyyy'),2,2);
insert into JOB_HISTORY (EMPLOYEE_ID,START_DATE,END_DATE,JOB_ID,DEPARTMENT_ID)
values (2,TO_DATE('24-11-2014', 'dd-MM-yyyy'),TO_DATE('24-11-2020', 'dd-MM-yyyy'),2,2);
insert into JOB_HISTORY (EMPLOYEE_ID,START_DATE,END_DATE,JOB_ID,DEPARTMENT_ID)
values (22,TO_DATE('04-10-2015', 'dd-MM-yyyy'),TO_DATE('03-12-2015', 'dd-MM-yyyy'),2,2);
insert into JOB_HISTORY (EMPLOYEE_ID,START_DATE,END_DATE,JOB_ID,DEPARTMENT_ID)
values (22,TO_DATE('04-12-2015', 'dd-MM-yyyy'),TO_DATE('04-12-2020', 'dd-MM-yyyy'),2,2);
insert into JOB_HISTORY (EMPLOYEE_ID,START_DATE,END_DATE,JOB_ID,DEPARTMENT_ID)
values (3,TO_DATE('20-09-2014', 'dd-MM-yyyy'),TO_DATE('20-08-2022', 'dd-MM-yyyy'),3,3);
insert into JOB_HISTORY (EMPLOYEE_ID,START_DATE,END_DATE,JOB_ID,DEPARTMENT_ID)
values (33,TO_DATE('10-10-2014', 'dd-MM-yyyy'),TO_DATE('20-08-2021', 'dd-MM-yyyy'),3,3);

-- ACTIVAR CONSTRAINT TABLA DEPARTMENTS
ALTER TABLE departments 
  ENABLE CONSTRAINT dept_mgr_fk;




