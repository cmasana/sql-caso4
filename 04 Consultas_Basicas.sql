/* Listado de cargos/tipos de empleo */
select * from JOBS;

/* Toda la información sobre cargo de contable: identificador y 
nombre del cargo, salario mínimo y máximo */
select * from JOBS
where JOB_TITLE='Contable';

/* Listado de trabajadores que cobran >=1500 ordenados por salario(ASC) */
select FIRST_NAME, LAST_NAME, SALARY from EMPLOYEES
WHERE SALARY>=1500
ORDER BY SALARY ASC;
