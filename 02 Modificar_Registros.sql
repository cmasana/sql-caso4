/* Modificar el num telf del trabajador Joan */
update EMPLOYEES set PHONE_NUMBER=222222222
where FIRST_NAME='Joan';

/* Modificar el num telf y el email del trabajador Joan */
update EMPLOYEES set PHONE_NUMBER=222222222, EMAIL='joanvila@montsia.org'
where FIRST_NAME='Joan';

/* Modificar el num telf del trabajador Joan */
update EMPLOYEES set PHONE_NUMBER=222222333
where FIRST_NAME='Joan'
AND LAST_NAME='Vila';
