/* Muestra la fecha del sistema */
select sysdate from employees;

/* Muestra el resultado de añadir 2 meses a la fecha especificada */
select data_tramesa, add_months(data_tramesa,2) from comanda
where com_num=610;

/* Muestra el último día del mes de la fecha especificada */
select data_tramesa, last_day(data_tramesa) from comanda
where com_num=610;

/* Muestra los meses transcurridos entre dos fechas */
select employee_id, months_between(end_date, start_date) from job_history
where employee_id=1;

/* Muestra el próximo Lunes tras la fecha especificada */
select com_num, com_data, next_day(com_data,'Lunes') from comanda
where com_num=601;
