/* Muestra el valor más grande y más pequeño de las columnas preu_venda
 * e import, en la comanda 613
 */
select com_num, greatest(preu_venda, import), least(preu_venda, import)
from detall where com_num=613;

/* Muestra el texto con más letras y con menos del trabajador
 * Pau Princep
 */
select employee_id, greatest(first_name, last_name), least(first_name, last_name)
from employees where employee_id=11;
