/* Obtenemos el nombre de todos los clientes y lo concatenamos con el texto 
'El nombre de los clientes es: ' y a continuación el nombre del cliente. */
SELECT CONCAT ('El nombre de los clientes es: ', NOM) FROM CLIENTS;

/* Mostramos en minúsculas, MAYÚSCULAS y Tipo Titulo, la cadena "TENNIS RACKETT" que 
se encuentra en la tabla productos. */
SELECT LOWER('TENNIS RACKET I') "conversion_a_minúscula", UPPER('tennis racket i') 
"CONVERSION_A_MAYÚSCULA",INITCAP('TeNNis RackET i') "Conversión_Tipo_Título" FROM PRODUCTE;

/* Mostramos la columna direccion de la tabla clientes. */
SELECT DIRECCIO FROM CLIENTS;

/* Mostramos la columna dirección de la tabla clientes, eliminando
el punto i final del final de la dirección. */
SELECT RTRIM (DIRECCIO, '.' ) FROM CLIENTS;

/* Mostramos la columna descripcion de la tabla productos. */
SELECT DESCRIPCIO FROM PRODUCTE;

/* Mostramos la columna descripcion de la tabla productos, eliminando
el punto i final del final de la descripcion. */
SELECT LTRIM (DESCRIPCIO, '"' ) FROM PRODUCTE;

/* Para cada fila del número de producto de la tabla DETALL, obtenemos el número
de producto con una longitud de 7 carácteres y rellenando por la izquierda con 
comillas y por la derecha con una longitud de 10 carácteres rellenandolo con 
puntos hacia la derehca. */
SELECT LPAD(PROD_NUM,7,'"') "IZQUIERDA", RPAD(PROD_NUM,10,'.')
"DERECHA" FROM DETALL;

/* Substituimos el "en" por el "an" de la cadena "Woman" */
SELECT REPLACE('WOMEN', 'EN', 'AN') FROM CLIENTS;

/* Si no ponemos nada para substituirlo, lo detecta como un null
y por tanto lo sustituye por nada. */
SELECT REPLACE('WOMEN', 'EN') FROM CLIENTS;

/* Partiendo de la cadena 'Springfield'obtenemos en una columna dos caracteres
a partir de la posición 7, en la otra columna obtenemos cuatro carácteres
a partir de la posición 7 empezando por el final y por último a partir de la 
posición 7 mostramos todo. */
SELECT SUBSTR('SPRINGFIELD',7,2) "Substitución1", SUBSTR ('SPRINGFIELD',-7,4) "Substitución2",
SUBSTR('SPRINGFIELD',7) "Substitución3" FROM CLIENTS;

/* Mostramos todos los clientes que su area sea 415 y su primera letra. */
SELECT NOM, SUBSTR(NOM,1,1) FROM CLIENTS WHERE AREA =415;

/* A partir de la cadena 'NORTH WOODS HEALTH AND FITNESS SUPPLY CENTER' sustituye
la 'O' por 'o', la 'T' por 't',la 'S' por 's', la 'A' por 'a', y la 'N' por 'n' */
SELECT TRANSLATE('NORTH WOODS HEALTH AND FITNESS SUPPLY CENTER','OTSAN','otsan') FROM CLIENTS;

/* Obtenemos el valor ASCII de 'U', de la tabla clientes. */
SELECT ASCII ('U') FROM CLIENTS;

/* Mostramos las ocurrencias de 'PE' empezando por la posicion 1. */
SELECT INSTR('PEPE PEREZ','PE',1,2) FROM DUAL; 

/* Mostramos las ocurrencias de 'PE' empezando por la posicion 2. */
SELECT INSTR('PEPE PEREZ','PE',2,2) FROM DUAL;

/* Esta consulta no termina de funcionar bien ya que no siempre  empieza a contar
por la posicion indicada. */


