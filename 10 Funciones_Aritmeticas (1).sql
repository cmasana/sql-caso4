/* Listado con la diferencia de salario mínimo y máximo de cada cargo*/
select job_title, min_salary, max_salary, abs(min_salary - max_salary) "Diferencia"
from jobs;

/* Redondea hacia arriba el importe de la comanda 601 */
select prod_num, ceil(import) from detall
where com_num=601;

/* Redondea hacia abajo el importe de la comanda 601 */
select prod_num, floor(import) from detall
where com_num=601;

/* Muestra el resto de la división del importe entre la cantidad */
select prod_num, mod(import,quantitat) from detall
where com_num=620;

/* Muestra el valor del importe elevado al valor de la cantidad */
select prod_num, power(import,quantitat) from detall
where com_num=610;

/* Redondea los decimales del precio de venta de un producto al valor indicado,
 * en la comanda 618
 */
select prod_num, round(preu_venda,1) from detall
where prod_num=100861
AND com_num=618;

/* Redondea desde el primer valor entero,
 * ya que el valor introducido es negativo
 */
select prod_num, round(preu_venda,-1) from detall
where prod_num=100861
AND com_num=618;

/* Trunca los decimales del precio de venta de un producto al valor indicado,
 * en la comanda 618
 */
select prod_num, trunc(preu_venda,1) from detall
where prod_num=100861
AND com_num=618;

/* Trunca desde el entero,
 * ya que el valor introducido es negativo
 */
select prod_num, trunc(preu_venda,-1) from detall
where prod_num=100861
AND com_num=618;

/* Muestra la raíz cuadrada del precio de venta */
select prod_num, sqrt(preu_venda) from detall
where prod_num=100861
AND com_num=618;

/* Muestra el coseno del precio de venta */
select prod_num, cos(preu_venda) from detall
where prod_num=100861
AND com_num=618;

/* Muestra el logaritmo natural del precio de venta */
select prod_num, ln(preu_venda) from detall
where prod_num=100861
AND com_num=618;
