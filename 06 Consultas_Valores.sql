/* Listado de todos los trabajadores que no tengan una comisión */
SELECT first_name FROM employees WHERE commission_pct IS NULL;


/* Insertamos un empleado con una comisión, ya que no hay ninguno. */
insert into EMPLOYEES (EMPLOYEE_ID, FIRST_NAME, LAST_NAME, EMAIL, PHONE_NUMBER, HIRE_DATE, JOB_ID, SALARY, COMMISSION_PCT, MANAGER_ID, DEPARTMENT_ID)
values (13, 'Xavier', 'Martinez', 'xavier@montsia.org', '453678543', TO_DATE('29-07-2015', 'dd-MM-yyyy'), '1', 1500, 0.15, NULL, 1);


/* Listado de todos los trabajadores que tengan una comisión */
SELECT first_name FROM employees WHERE commission_pct IS NOT NULL;
