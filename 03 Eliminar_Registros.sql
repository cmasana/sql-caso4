/* Borrar el registro de un usuario */
delete from EMPLOYEES where FIRST_NAME='María';

/* Borrar el registro de un usuario indicando su nombre y su apellido */
delete from EMPLOYEES where FIRST_NAME='Secundino'
AND LAST_NAME='Grau';

/* Borrar todos los registros de la tabla empleados */
truncate table EMPLOYEES cascade;
 
/* Eliminar dependencias de las tablas con fk que
 * hacen referencia a la tabla EMPLOYEES
 */
ALTER TABLE DEPARTMENTS
    DROP CONSTRAINT DEPT_MGR_FK;

ALTER TABLE DEPARTMENTS
    ADD CONSTRAINT DEPT_MGR_FK
    FOREIGN KEY ( MANAGER_ID )
    REFERENCES EMPLOYEES ( EMPLOYEE_ID )
    ON DELETE CASCADE;

ALTER TABLE EMPLOYEES
    DROP CONSTRAINT EMP_MANAGER_FK;

ALTER TABLE EMPLOYEES
    ADD CONSTRAINT EMP_MANAGER_FK
    FOREIGN KEY ( MANAGER_ID )
    REFERENCES EMPLOYEES ( EMPLOYEE_ID )
    ON DELETE CASCADE;

ALTER TABLE JOB_HISTORY
    DROP CONSTRAINT JHIST_EMP_FK;

ALTER TABLE JOB_HISTORY
    ADD CONSTRAINT JHIST_EMP_FK
    FOREIGN KEY ( EMPLOYEE_ID )
    REFERENCES EMPLOYEES ( EMPLOYEE_ID )
    ON DELETE CASCADE;
